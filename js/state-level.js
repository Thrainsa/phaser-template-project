define(['consts', 'engine/save-state', 'engine/tilemap', 'entities/player', 'entities/monster', 'entities/point', 'entities/obstacle', 'background', 'entities/shutter', 'engine/forms'],
	   function(Consts, SaveState, FactoryTilemap, FactoryPlayer, FactoryMonster, FactoryPoint, FactoryObstacle, Background, Shutter, Forms) {

	var state = new Phaser.State();

	var text;

	var themeSound;

	state.preload = function() {
		var debugLevel = null;

		this.levelInfo = {};
		this.levelInfo.map;
		this.levelInfo.level = debugLevel ? debugLevel[0] : SaveState.level;
		this.levelInfo.subLevel = debugLevel ? debugLevel[1] : SaveState.subLevel;
		this.levelInfo.unlockedCharacters = SaveState.unlockedCharacters;
		this.levelInfo.tilesets = ["tileset", "objects"];
		this.game.stage.backgroundColor = '#0a1012';
		var levelId = 'level' + this.levelInfo.level + '-' + this.levelInfo.subLevel;
		this.load.tilemap(levelId , 'tilemaps/maps/' + levelId + '.json', null, Phaser.Tilemap.TILED_JSON);
		
		FactoryPlayer.preload(this);
	};
	
	state.create = function() {
		/*
		if (!themeSound) {
			themeSound = state.add.audio('theme');
		}
		if (!themeSound.isPlaying && !SaveState.mute) {
			themeSound.play('', 0, 0.7, true);
		}*/

		this.game.physics.startSystem(Phaser.Physics.P2JS);
		this.game.physics.p2.setImpactEvents(true);

		this.groups = {
			map: this.game.physics.p2.createCollisionGroup(),
			player: this.game.physics.p2.createCollisionGroup(),
			enemies: this.game.physics.p2.createCollisionGroup(),
			obstacles: this.game.physics.p2.createCollisionGroup(),
			points: this.game.physics.p2.createCollisionGroup(),
			fruits: this.game.physics.p2.createCollisionGroup()
		};

		this.bg = Background.create(this);

		// Chargement de la carte
		this.loadMap(this.levelInfo.level, this.levelInfo.subLevel);
		this.game.camera.follow(state.player);

		this.shutter = Shutter.create(this);

		this.game.physics.p2.pause();

		// Mute button
		var muteKey = state.add.image(Consts.WIDTH - 50, Consts.HEIGHT-60, 'keysmall');
		muteKey.fixedToCamera = true;
		var mText = state.add.text(Consts.WIDTH - 40, Consts.HEIGHT-53, 'M', { font: "14px sans-serif"});
		mText.fixedToCamera = true;
		var muteText = state.add.text(Consts.WIDTH - 90, Consts.HEIGHT-53, 'Mute', { font: "14px sans-serif"});
		muteText.fixedToCamera = true;
		var mKey = state.input.keyboard.addKey(Phaser.Keyboard.M);
		mKey.onDown.add(function() {
			SaveState.mute = !SaveState.mute;
			SaveState.save();
			if (SaveState.mute) {
				themeSound.volume = 0;
			}
			else {
				themeSound.volume = 1;
			}
		});

		this.shutter.open(function(){
			var firstLevel = state.levelInfo.level == 1 && state.levelInfo.subLevel == 1;
			setTimeout(function() {
				state.game.physics.p2.resume();
				
				state.levelInfo.levelTxt = state.game.add.text(830, 25, "Level "+ state.levelInfo.level + '-' + state.levelInfo.subLevel, { font: "28px Comic Sans Ms", fill: "#000000", align: "right" });
				state.levelInfo.levelTxt.fixedToCamera = true;
				
				state.levelInfo.pointsImg = state.game.add.sprite(25, 20, 'point_ui');
				state.levelInfo.pointsImg.fixedToCamera = true;
				state.levelInfo.pointsTxt = state.game.add.text(60, 25, state.player.info.points, { font: "28px Comic Sans Ms", fill: "#000000", align: "left" });
				state.levelInfo.pointsTxt.fixedToCamera = true;
				
				state.levelInfo.escKey = state.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
				state.levelInfo.pKey = state.game.input.keyboard.addKey(Phaser.Keyboard.P);
				state.levelInfo.qKey = state.game.input.keyboard.addKey(Phaser.Keyboard.Q);
				state.levelInfo.escKey.onDown.add(state.togglePause, state);
				state.levelInfo.pKey.onDown.add(state.togglePause, state);
				state.levelInfo.qKey.onDown.add(state.quitToMenu, state);
			}, firstLevel ? 1000 : 0);
		});
    };

	state.update = function() {
		this.bg.offset(this.game.camera.x, this.game.camera.y);
		
		if (this.player ) {
            this.player.update();
			
			// Load next map
			if (this.player.body.x > this.levelInfo.map.widthInPixels - 300 && SaveState.subLevel == state.levelInfo.subLevel && SaveState.level == state.levelInfo.level) {
				if (Consts.DEBUG.TEST_COLLISION_MAP == true) {
					state.game.state.start("level");
					return;
				}
				
				SaveState.subLevel += 1;
				if (SaveState.subLevel > Consts.LEVELS[state.levelInfo.level - 1].SUBLEVELS) {
					SaveState.level++;
					SaveState.subLevel = 1;
				}
				SaveState.unlockedCharacters = this.levelInfo.unlockedCharacters;
				SaveState.points = this.player.info.points;
				SaveState.save();
				this.shutter.close(function(){
					this.isSwitchingLevels = false;
					if (SaveState.level > _.size(Consts.LEVELS)) {
						SaveState.victory = true;
						state.game.state.start("leaderboard");
					}
					else {
						state.game.state.start("level");
					}
				});
			}

			// Mort lorsque l'on tombe dans le vide
			if ((this.player.body.x < this.game.camera.x || this.player.body.y > this.player.info.state.world.height) && !this.player.dead) {
                this.player.death();
            }
        }
    };

    state.render = function() {
    };
	
	state.death = function(){
		this.game.physics.p2.pause();

		this.shutter.close(function(){
			state.game.state.start("level");
		});
	}
	
	state.collectNewPoints = function(points){
		this.levelInfo.pointsTxt.setText(points);
	}

	state.shutdown = function(){
		FactoryPlayer.destroy(this.player);
	};
	
	state.loadMap = function(levelId, checkPointId){
		var groupsToCollideWith = [
			state.groups.player,
			state.groups.enemies,
			state.groups.obstacles
		];

		this.levelInfo.map = FactoryTilemap.load(this, 'level' + levelId + '-' + checkPointId, groupsToCollideWith);

		for (var type in this.levelInfo.objects) {
			var objects = this.levelInfo.objects[type];
			for (var i=0; i < objects.length; i++) {
				var data = objects[i];
				if (type == "player") {
                    state.player = FactoryPlayer.create(state, data);
                } else if (type == "monster") {
                    FactoryMonster.create(state, data);
                } else if (type == "point") {
                    FactoryPoint.create(state, data);
                } else if (type == "obstacle") {
                    FactoryObstacle.create(state, data);
                }
			};
		}
	}
	
	state.togglePause = function(characterId) { // XXX other params are set on keydown, we have to check if the value is valid later on
		if (!this.player.dead) {
			if (!this.levelInfo.paused) {
				this.game.physics.p2.pause();
				this.levelInfo.paused = true;

				this.showPauseMenu();
				
				// Handle key to change menu
				
			} else {
				this.pauseCharacter = null;
				this.game.physics.p2.resume();
				this.levelInfo.paused = false;
				Forms.getActive().hide();
			}
		}
    };
	
	var currentVoice = null;
	state.showPauseMenu = function() {
		var formData = {};
		var form = Forms.load('pause', formData);
		form.show();
	}
	
	state.quitToMenu = function(){
		if (this.levelInfo.paused) {
			Forms.getActive().hide();
			state.sound.stopAll();
			state.game.state.start("menu");
		}
	}
	
	return state;

});
