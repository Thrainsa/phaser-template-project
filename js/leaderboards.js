define(['consts', 'base64'], function(Consts) {

//    ,..__
//   |  _  `--._                                  _.--"""`.
//   |   |._    `-.        __________         _.-'    ,|' |
//   |   |  `.     `-..--""_.        `""-..,-'      ,' |  |
//   L   |    `.        ,-'                      _,'   |  |
//    .  |     ,'     ,'            .           '.     |  |
//    |  |   ,'      /               \            `.   |  |
//    |  . ,'      ,'                |              \ /  j
//    `   "       ,                  '               `   /
//     `,         |                ,'                  '+
//     /          |             _,'                     `
//    /     .-"""'L          ,-' \  ,-'""""`-.           `
//   j    ,' ,.+--.\        '    ',' ,.,-"--._`.          \
//   |   / .'  L    `.        _.'/ .'  |      \ \          .
//  j   | | `--'     |`+-----'  . j`._,'       L |         |
//  |   L .          | |        | |            | |         |
//  |   `\ \        / j         | |            | |         |
//  |     \ `-.._,.- /           . `         .'  '         |
//  l      `-..__,.-'             `.`-.....-' _.'          '
//  '                               `-.....--'            j
//   .                  -.....                            |
//    L                  `---'                            '
//     \                                                 /
//      ` \                                        ,   ,'
//       `.`.    |                        /      ,'   .
//         . `._,                        |     ,'   .'
//          `.                           `._.-'  ,-'
//     _,-""""`-,                             _,'"-.._
//   ,'          `-.._                     ,-'        `.
//  /             _,' `"-..___     _,..--"`.            `.
// |         _,.-'            `"""'         `-._          \
// `-....---'                                   `-.._      |
//                                                   `--...' mh
//    ,..__
//   |  _  `--._                                  _.--"""`.
//   |   |._    `-.        __________         _.-'    ,|' |
//   |   |  `.     `-..--""_.        `""-..,-'      ,' |  |
//   L   |    `.        ,-'                      _,'   |  |
//    .  |     ,'     ,'            .           '.     |  |
//    |  |   ,'      /               \            `.   |  |
//    |  . ,'      ,'                |              \ /  j
//    `   "       ,                  '               `   /
//     `,         |                ,'                  '+
//     /          |             _,'                     `
//    /     .-"""'L          ,-' \  ,-'""""`-.           `
//   j    ,' ,.+--.\        '    ',' ,.,-"--._`.          \
//   |   / .'  L    `.        _.'/ .'  |      \ \          .
//  j   | | `--'     |`+-----'  . j`._,'       L |         |
//  |   L .          | |        | |            | |         |
//  |   `\ \        / j         | |            | |         |
//  |     \ `-.._,.- /           . `         .'  '         |
//  l      `-..__,.-'             `.`-.....-' _.'          '
//  '                               `-.....--'            j
//   .                  -.....                            |
//    L                  `---'                            '
//     \                                                 /
//      ` \                                        ,   ,'
//       `.`.    |                        /      ,'   .
//         . `._,                        |     ,'   .'
//          `.                           `._.-'  ,-'
//     _,-""""`-,                             _,'"-.._
//   ,'          `-.._                     ,-'        `.
//  /             _,' `"-..___     _,..--"`.            `.
// |         _,.-'            `"""'         `-._          \
// `-....---'                                   `-.._      |
//                                                   `--...' mh
//    ,..__
//   |  _  `--._                                  _.--"""`.
//   |   |._    `-.        __________         _.-'    ,|' |
//   |   |  `.     `-..--""_.        `""-..,-'      ,' |  |
//   L   |    `.        ,-'                      _,'   |  |
//    .  |     ,'     ,'            .           '.     |  |
//    |  |   ,'      /               \            `.   |  |
//    |  . ,'      ,'                |              \ /  j
//    `   "       ,                  '               `   /
//     `,         |                ,'                  '+
//     /          |             _,'                     `
//    /     .-"""'L          ,-' \  ,-'""""`-.           `
//   j    ,' ,.+--.\        '    ',' ,.,-"--._`.          \
//   |   / .'  L    `.        _.'/ .'  |      \ \          .
//  j   | | `--'     |`+-----'  . j`._,'       L |         |
//  |   L .          | |        | |            | |         |
//  |   `\ \        / j         | |            | |         |
//  |     \ `-.._,.- /           . `         .'  '         |
//  l      `-..__,.-'             `.`-.....-' _.'          '
//  '                               `-.....--'            j
//   .                  -.....                            |
//    L                  `---'                            '
//     \                                                 /
//      ` \                                        ,   ,'
//       `.`.    |                        /      ,'   .
//         . `._,                        |     ,'   .'
//          `.                           `._.-'  ,-'
//     _,-""""`-,                             _,'"-.._
//   ,'          `-.._                     ,-'        `.
//  /             _,' `"-..___     _,..--"`.            `.
// |         _,.-'            `"""'         `-._          \
// `-....---'                                   `-.._      |
//                                                   `--...' mh

// http://stackoverflow.com/questions/10232017/ie9-jquery-ajax-with-cors-returns-access-is-denied
if ( window.XDomainRequest ) {
	jQuery.ajaxTransport(function( s ) {
		if ( s.crossDomain && s.async ) {
			if ( s.timeout ) {
				s.xdrTimeout = s.timeout;
				delete s.timeout;
			}
			var xdr;
			return {
				send: function( _, complete ) {
					function callback( status, statusText, responses, responseHeaders ) {
						xdr.onload = xdr.onerror = xdr.ontimeout = jQuery.noop;
						xdr = undefined;
						complete( status, statusText, responses, responseHeaders );
					}
					xdr = new XDomainRequest();
					xdr.onload = function() {
						callback( 200, "OK", { text: xdr.responseText }, "Content-Type: " + xdr.contentType );
					};
					xdr.onerror = function() {
						callback( 404, "Not Found" );
					};
					xdr.onprogress = jQuery.noop;
					xdr.ontimeout = function() {
						callback( 0, "timeout" );
					};
					xdr.timeout = s.xdrTimeout || Number.MAX_VALUE;
					xdr.open( s.type, s.url );
					xdr.send( ( s.hasContent && s.data ) || null );
				},
				abort: function() {
					if ( xdr ) {
						xdr.onerror = jQuery.noop;
						xdr.abort();
					}
				}
			};
		}
	});
}

	return {

		fetch: fetch, // (callack) renvoie un tableau de scores {name, score}
		submitScore: submitScore // (name, score, callback)

	};

	function fetch(callback) {
		$.getJSON('http://' + Consts.SERVER_HOST + '?callback=?&id=' + Consts.LEADERBOARD_ID, callback)
			.fail(function (e) {
				console.error(JSON.stringify(e));
				callback();
			});
	}

	// Please don't hack us! http://i.imgur.com/6CYdOcW.jpg
	function submitScore(name, score, callback) {
		$.get('http://' + Consts.SERVER_HOST + '?callback=?&data=' + nothingSpecial(name, score), callback)
			.fail(function (e) {
				if (e.status != 200) {
					console.error(JSON.stringify(e));
				}
				callback();
			});
	}

	function nothingSpecial(name, score) {
		return Base64.encode(Base64.encode(Base64.encode(
			JSON.stringify({id:Consts.LEADERBOARD_ID,name:name,score:score,timestamp:Date.now()}))));
	}

})