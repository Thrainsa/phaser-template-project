define([], function() {

	return {

		DEBUG : {
			TEST_COLLISION_MAP: false,
			SKIP_INTRO_CUTSCENE: false,
			SKIP_MENU: false,
			MUTE: false,
			SHOW_BODIES: false,
			DISABLE_SHUTTER: false,
			TEST_SUBMIT_LEADERBOARD: 0,
			FORCE_LEVEL: null
		},
		
		PLAYER : {
			STANCES : {
				"idle": {
					texture: "player-idle",
					body: {
						width: 70,
						height: 50,
						offset: {
							x: -20
						}
					},
					animations: [
						{name: "idle"}
					]
				},
				"walk": {
					texture: "player-walk",
					body: {
						width: 70,
						height: 50,
						offset: {
							x: -20
						}
					},
					animations: [
						{name: "walk"},
					]
				},
				"jump": {
					texture: "player-jump",
					body: {
						width: 70,
						height: 50,
						offset: {
							x: -20,
							y: 5
						}
					},
					animations: [
						{name: "jump", frames : [0,1]},
						{name: "fall", frames : [2,3]}
					]
				}
			}
		},

		WIDTH: 960,
		HEIGHT: 600,
		TILESIZE: 96,

		SPEED: 400,
		GRAVITY: 2000,

		PLAYER_X: 300,
		LATE_PLAYER_ACCELERATION: 0.3,

		JUMP_FORCE: 900,
		
		TERRAIN: {
			GROUND: "ground",
			AIR: "air",
			WATER: "water"
		},
		
		LEVELS: {
			0: {
				SUBLEVELS: 1
			}
		},
		
		HTML: {
			KEYS: {
				1: "&#x21E7;",
				2: "&#x21E6;",
				3: "&#x21E8;",
				4: "&#x21E9;"
			}
		},
		SAVE_STATE_DEFAULT: {
			gameName: "save1",
        	playerName: "New player",
			level: 1,
			subLevel : 1,
			points: 0
        }
		
	};

});
