define(['consts','engine/save-state',  'engine/utils'],
	   function(Consts, SaveState, Utils) {

	return {
		preload: preload,
		create: create,
		destroy: destroy
	};

	function preload(state) {

	}
	
	function create(state, playerInfo) {
		var player = state.game.add.sprite(playerInfo.x, playerInfo.y, 'player-idle');
		
		// correction position body
		Utils.attachObjectToGrid(player,true, Consts.TILESIZE);
				
		// Physics
		state.game.physics.p2.enable(player, Consts.DEBUG.SHOW_BODIES);
		player.body.fixedRotation = true;	
		
		// correction position body
		Utils.replaceSprite(player);
		
		player.info = {};
		player.info.state = state;
		player.info.points = SaveState.points || 0;
		
		// Keyboard binding
		var keyboard = state.game.input.keyboard;
		var upKey = keyboard.addKey(Phaser.Keyboard.UP);
		var downKey = keyboard.addKey(Phaser.Keyboard.DOWN);
		var leftKey = keyboard.addKey(Phaser.Keyboard.LEFT);
		var rightKey = keyboard.addKey(Phaser.Keyboard.RIGHT);
		
		player.currentTerrain = null;
		player.isFalling = false;
		player.idle = false;

		// Input
		player.update = function() {	
			if (rightKey.isDown){
				speedScale = 1;
			} else if (leftKey.isDown) {
				speedScale = -1;
			} else{
				speedScale = 0;
			}
			
			this.body.velocity.x = Consts.SPEED * speedScale;
			if((this.body.velocity.x < 0 && this.scale.x > 0)
				|| this.body.velocity.x > 0 && this.scale.x < 0){
				this.scale.x *= -1;
				this.body.offset.x *= -1;
				this.body.x = this.body.x - 2 * (this.body.offset.x);
			}
			
			// Load Animation en fonction du terrain
			var terrain = this.getCurrentTerrain();
			if (terrain != this.currentTerrain || (this.idle && Math.abs(this.body.velocity.x) > 0)) {
				this.idle = false;
                // Changement de terrain => Changement animation
                this.currentTerrain = terrain;
                if (terrain != Consts.TERRAIN.AIR) {
					player.switchStance(Consts.PLAYER.STANCES["walk"]);
                    this.animations.play("walk", 10, true);
                }
                else {
					player.switchStance(Consts.PLAYER.STANCES["jump"]);
                    if (this.body.velocity.y >= 0) {
                        this.animations.play("fall", 10, false);
                        this.isFalling = true;
                    }
                    else {
                        this.animations.play("jump", 10, true);
                        this.isFalling = false;
                    }
                }
            } else if (terrain == Consts.TERRAIN.AIR
                    && !this.isFalling && this.body.velocity.y >= 0) {
				this.idle = false;
                this.animations.play("fall", 10, false);
                this.isFalling = true;
            } else if(terrain != Consts.TERRAIN.AIR && this.body.velocity.x == 0 && !this.idle){
				player.switchStance(Consts.PLAYER.STANCES["idle"]);
                this.animations.play("idle", 10, true);
				this.idle = true;
			}

			// jump
			if (upKey.isDown && this.checkIfCanJump()) {
				this.body.moveUp(Consts.JUMP_FORCE);
				this.animations.play("jump", 5, false);
			}
			
		};
		
		player.getCurrentTerrain = function(){
			var contactEquations = this.game.physics.p2.world.narrowphase.contactEquations;
			var yAxis = p2.vec2.fromValues(0,1);
			var result = false;
		
			for (var i=0; i < contactEquations.length; i++) {
				var c = contactEquations[i];
				// If it's a player collision and with something else than the world borders
				if ((c.bodyA === this.body.data || c.bodyB === this.body.data) && (c.bodyA.parent != null && c.bodyB.parent != null))
				{
					var d = p2.vec2.dot(c.normalA, yAxis);
					var terrain;
					if (c.bodyA === player.body.data){
						d *= -1;
						terrain = c.bodyB.parent.properties.terrain;
					} else {
						terrain = c.bodyA.parent.properties.terrain;
					}

					if (d > 0.5 && terrain) {
						return terrain;
					}
				}
			}
			// Player in the air if nothing else
			return Consts.TERRAIN.AIR;
		}
		
		player.switchStance = function(stance) {
			if (stance != this.currentStance) {
				var direction = this.scale.x < 0 ? -1 : 1;
				this.currentStance = stance;

				var posX = this.body.x;
				var posY = this.body.y;
			
				this.loadTexture(stance.texture, 0, true);
				stance.animations.forEach(function(animation) {
					this.animations.add(animation.name, animation.frames || null);
				}, this);
		
				// Physics
				//this.body.addPolygon( {} , stance.polygon);
				this.body.clearShapes();
				this.body.addRectangle(stance.body.width,stance.body.height, 0, 0);
				
				this.body.offset.x = stance.body.offset.x || 0;
				this.body.offset.y = stance.body.offset.y || 0;
				
				if(direction < 0){
					this.body.offset.x *= -1;
					this.body.x = this.body.x - 2 * (this.body.offset.x);
				}
				
				this.body.x = posX;
				this.body.y = posY;
				
				
				this.body.setCollisionGroup(state.groups.player);
				
				
			//	this.body.collides(state.groups.enemies, collideWithEnemy, this);
				this.body.collides(state.groups.points);
			//	this.body.collides(state.groups.obstacles, this);
				this.body.collides(state.groups.map);
				
				this.body.data.gravityScale = stance.gravityScale || 1;
			}
		}
		
		player.checkIfCanJump = function(){
			var yAxis = p2.vec2.fromValues(0,1);
		
			for (var i=0; i < this.game.physics.p2.world.narrowphase.contactEquations.length; i++)
			{
				var c = this.game.physics.p2.world.narrowphase.contactEquations[i];
		
				if (c.bodyA === this.body.data || c.bodyB === this.body.data)
				{
					var d = p2.vec2.dot(c.normalA, yAxis);
					if (c.bodyA === this.body.data)
					{
						d *= -1;
					}
		
					if (d > 0.5)
					{
						return true;
					}
				}
			}
			return false;
		}
		
		player.death = function(){
			this.dead = true;
			player.info.state.death();
		}
		
		player.collectPoints = function(point){
			this.info.points += point.getValue();
			this.info.state.collectNewPoints(this.info.points);
			flashAnimation(this.info.state, point.body, 0.2);
			point.destroy();
		}
		
		return player;
	}
	
	function collideWithEnemy(playerBody, enemyBody){
		if (enemyBody && enemyBody.sprite.death != true) {
			this.rotation = -1.7;
			this.info.state.add.tween(this)
				.to({rotation: -3.4, alpha: 0}, 500)
				.start();
			this.info.state.add.tween(this.body)
				.to({x: this.body.x - 100, y: this.body.y - 60}, 500)
				.start();
			this.animations.stop();
			this.death();
		}
	}

	function flashAnimation(state, sprite, scale) {
		// animation
		if (!SaveState.mute) {
			// state.add.sound(scale >= 0.3 ? 'start' : 'click').play();
		}
		var flash = state.add.sprite(sprite.x, sprite.y, 'flash');
		flash.anchor.set(0.5);
		flash.scale.set(scale || 0.3);
		state.add.tween(flash)
			.to({alpha: 0, rotation: 50}, 500)
			.start();
	}

	function smokeAnimation(state, player) {
		if (!SaveState.mute) {
		//	state.add.sound('start).play();
		}
		var smoke = state.add.sprite(player.x, player.y, 'smoke');
		smoke.anchor.set(0.5);
		smoke.scale.set(player.width/smoke.width);
		state.add.tween(smoke)
			.to({alpha: 0, rotation: 50}, 300)
			.start();
		state.add.tween(smoke.scale)
			.to({x: smoke.scale.x * 1.5, y: smoke.scale.x * 1.5}, 300)
			.start();
	}
		

	function destroy(player) {
	};

});
