define(['consts', 'engine/utils'], function(Consts, Utils) {
	return {
		create: create
	};

	function create(state, data) {
		var point = state.game.add.sprite(data.x, data.y, data.properties.name);
		if (data.properties.scale) {
			data.properties.scale = parseInt(data.properties.scale);
            point.scale = new PIXI.Point(data.properties.scale, data.properties.scale);
        }
		data.properties.value = parseInt(data.properties.value);
		point.info = data;
		
		state.game.physics.p2.enable(point, Consts.DEBUG.SHOW_BODIES);
		
		point.body.setCollisionGroup(state.groups.points);
		point.body.collides(state.groups.player);
		point.body.onBeginContact.add(playerCollision, point);

		// correction position body
		Utils.replaceSprite(point);
		point.body.data.gravityScale = 0;
	
		point.body.data.shapes[0].sensor=true
	
		// Animation
		
		// Audio

		point.update = function() {
		};
		
		point.getValue = function(){
			return data.properties.value;
		}

		return point;
	}
	
		function playerCollision(playerBody){
			playerBody.sprite.collectPoints(this);
		}
});
