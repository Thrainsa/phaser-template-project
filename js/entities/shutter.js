define(['consts'], function(Consts) {

	var HALF_HEIGHT = Consts.HEIGHT/2;
	var DEFAULT_DELAY = 1000;
	var CINEMA_MODE_OFFSET = 70;

	var Shutter = function(state, open) {
		this._state = state;
		this.top = state.add.image(0, 0, 'shutter');
		this.top.fixedToCamera = true;
		this.bottom = state.add.image(0, 0, 'shutter');
		this.bottom.fixedToCamera = true;
		if (open) {
			this.setOpen();
		}
		else {
			this.setClosed();
		}
		if (Consts.DEBUG.DISABLE_SHUTTER) {
			this.top.alpha = 0;
			this.bottom.alpha = 0;
		}
	}

	Shutter.prototype.open = function(callback, delay) {
		this._state.world.bringToTop(this.top);
		this._state.world.bringToTop(this.bottom);
		delay = (delay !== undefined) ? delay : DEFAULT_DELAY;
		this._moveTo(this.top, -HALF_HEIGHT, delay, callback);
		this._moveTo(this.bottom, HALF_HEIGHT*2, delay);
	}

	Shutter.prototype.cutsceneMode = function(callback, delay) {
		this._state.world.bringToTop(this.top);
		this._state.world.bringToTop(this.bottom);
		delay = (delay !== undefined) ? delay : DEFAULT_DELAY;
		this._moveTo(this.top, -HALF_HEIGHT+CINEMA_MODE_OFFSET, delay, callback);
		this._moveTo(this.bottom, HALF_HEIGHT*2-CINEMA_MODE_OFFSET, delay);
	}

	Shutter.prototype.close = function(callback, delay) {
		this._state.world.bringToTop(this.top);
		this._state.world.bringToTop(this.bottom);
		delay = (delay !== undefined) ? delay : DEFAULT_DELAY;
		this._moveTo(this.top, 0, delay, callback);
		this._moveTo(this.bottom, HALF_HEIGHT, delay);
	}

	Shutter.prototype.setClosed = function() {
		this.close(null, 0);
	}

	Shutter.prototype.setOpen = function() {
		this.open(null, 0);
	}

	Shutter.prototype._moveTo = function(sprite, y, delay, callback) {
		if (!Consts.DEBUG.DISABLE_SHUTTER) {
			this._state.world.bringToTop(sprite);
			if (delay > 0) {
				var tween = this._state.add.tween(sprite.cameraOffset);
				tween.to({y: y}, delay, Phaser.Easing.Quadratic.InOut);
				if (callback) {
					tween.onComplete.addOnce(callback);
				}
				tween.start();
			}
			else {
				sprite.cameraOffset.y = y;
				if (callback) {
					callback();
				}
			}
		}
		else if (callback) {
			callback();
		}
	}

	return {
		create: function(state) {
			return new Shutter(state);
		}
	}

})