define(['consts', 'background', 'entities/shutter', 'engine/forms', 'engine/tilemap', 'engine/chain', 'engine/save-state'], function(Consts, Background, Shutter, Forms, Tilemap, Chain, SaveState) {

	var state = new Phaser.State();

	state.preload = function() {
		this.levelInfo = {};
		this.levelInfo.tilesets = ["tileset", "objects"];
		this.load.tilemap('cutscene', 'tilemaps/maps/cutscene.json',
			null, Phaser.Tilemap.TILED_JSON);
	};

	state.create = function() {
		var outro = SaveState.victory;

		this.game.physics.startSystem(Phaser.Physics.P2JS);

		this.bg = Background.create(this);
		Tilemap.load(this, 'cutscene');

		this.chain = Chain.create();

		this.chain.add(function(next) {
			state.shutter = Shutter.create(state);
			state.shutter.cutsceneMode(function() {
				setTimeout(next, outro ? 500 : 1000);
			});
			/*
	    	state.musicSound = state.add.audio('cutscene');
	    	state.musicSound.fadeIn(2000, true);
			*/
		});

		state.levelInfo.escKey = state.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
		state.levelInfo.escKey.onDown.addOnce(function() {
			state.chain.kill();
			// state.musicSound.fadeOut(1000);
			if (state.form) {
				state.form.unload();
			}
			state.shutter.close(function() {
				if (outro) {
					state.game.state.start('menu');
				}
				else {
					state.game.state.start('level');
				}
			});
		});

		if (outro) {
			this.chain.add(function(next) {
				state.form = Forms.load('cutscene');
				state.form.hide();
				setText('Victory!!!', 4000, next);
			});
		}
		else {
			this.chain.add(function(next) {
				state.form = Forms.load('cutscene');
				state.form.hide();
				setText('Début, intro', 2500, next);
			});
		}

		this.chain.add(function(next) {
			//state.musicSound.fadeOut(1000);
			state.form.hide(200, next);
		});

		this.chain.add(function(next) {
			state.shutter.close(next);
		});

		this.chain.add(function(next) {
			if (outro) {
				state.game.state.start('menu')
			}
			else {
				state.game.state.start('level');
			}
		});

		this.chain.start();

	};

	function setText(text, delay, callback) {
		state.form.hide(200, function() {
			state.form.getElement('text').html(text);
			setTimeout(function() {
				state.form.show(200, function() {
					setTimeout(callback, delay);
				});
			}, 500);
		})
	}

	return state;

});
