define(['consts', 'background', 'entities/shutter', 'engine/forms', 'engine/chain', 'engine/save-state'],
	   function(Consts, Background, Shutter, Forms, Chain, SaveState) {

	var state = new Phaser.State();

	state.create = function() {

		this.bg = Background.create(this);

		var logo = this.add.image(Consts.WIDTH / 2, 130, 'logo');
		logo.anchor.set(0.5);
		//logo.angle = -3;
		SaveState.mute = true;
		if (!SaveState.mute) {
			state.add.audio('menu').play('', 0, 0.5, true);
		}

		this.inChain = Chain.create();

		this.inChain.add(function(next) {
			state.shutter = Shutter.create(state);
			state.shutter.open(next);
		});

		this.inChain.add(function(next) {
			state.form = Forms.load('menu');
			state.form.show();
			state.inputEnabled = true;
		});

		this.inChain.start();

		this.outChain = Chain.create();

		this.outChain.add(function(next) {
			state.form.hide(300, next);
		});

		this.outChain.add(function(next) {
			state.form.hide(200);
			state.shutter.close(next);
		});

		this.input.keyboard.addKey(Phaser.Keyboard.R);
	};

	state.update = function() {
		this.bg.offset(this.time.time / 10);

		if (this.inputEnabled) {
			if (this.input.keyboard.isDown(Phaser.Keyboard.UP)) {
				// reset current game
				SaveState.reset(Consts.SAVE_STATE_DEFAULT);
				SaveState.jumpToMenu = true;
				SaveState.save();
				switchToScene('cutscene');
			}
			if (this.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
				switchToScene('level');
			}
		}
	}

	function switchToScene(id) {
		state.sound.stopAll();
		if (!SaveState.mute) {
			/*
			state.add.audio('start').play('', 0, 0.7);
			*/
		}
		state.outChain.add(function() {
			setTimeout(function() {
				state.game.state.start(id);
			}, 1000);
		});
		state.outChain.start();
	}

	return state;

});
