define([], function() {

	return {
		attachObjectToGrid: attachObjectToGrid,
		replaceSprite: replaceSprite,
	};

    function attachObjectToGrid(sprite, yGrid, tileHeight, xGrid, tileWidth) {
        xGrid = (xGrid !== undefined) ? xGrid : false;
        yGrid = (yGrid !== undefined) ? yGrid : false;
        
        if (xGrid) {
            sprite.position.x = Math.round(sprite.position.x / tileWidth) * tileWidth;
        }
        if (yGrid) {
            sprite.position.y = Math.round(sprite.position.y / tileHeight) * tileHeight;
        }
    }
	
	function replaceSprite(sprite) {
        sprite.body.x += sprite.width/2;
		sprite.body.y -= sprite.height/2;
    }
});
