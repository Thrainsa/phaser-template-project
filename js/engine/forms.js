define([], function() {

	var $phaser = $('#phaser');
	var $form = $('#phaser-form');

	var activeForm = null;

	init();

	// Form class

	var Form = function(id) {
		this.id = id;
		if ($('#' + id).length == 0) console.error('no such form: ' + id);
		this.template = _.template($('#' + id).html());
	}
	Form.prototype.show = function(fadeDelay, callback) {
		$form.fadeIn(fadeDelay || 0, callback);
	}
	Form.prototype.hide = function(fadeDelay, callback) {
		$form.fadeOut(fadeDelay || 0, callback);
	}
	Form.prototype.unload = function() {
		$form.html('');
	}
	Form.prototype.getElement = function(id) {
		return $('#' + id, $form);
	}
	Form.prototype.query = function(query) {
		return $(query, $form);
	}

	return {
		// Call after initializing Phaser to make sure the forms adapt to the game size & position.
		// refresh()
		refresh: refresh,

		// Loads a Form given its id and a context to provide to the template
		// load(id, vars)
		load: load,

		// Get the current form
		// getActive()
		getActive: getActive
	};

	// Functions

	function init() {
		$(window).resize(refresh);
	}

	function refresh() {
		$form.css('left', ($phaser.position().left - $form.width()/2) + 'px');
		$form.css('top', ($phaser.position().top - $form.height()/2) + 'px');
	}

	function load(id, vars) {
		activeForm = new Form(id);
		$form.html(activeForm.template(vars));
		refresh();
		return activeForm;
	}

	function getActive() {
		return activeForm;
	}

});