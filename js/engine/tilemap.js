define(['consts', 'engine/save-state'], function(Consts, SaveState) {

	return {
		load: load
	};

	/**
	Object types:
	- block [default] (magnet: bool, locked: bool)
	- player
	- monster (type: string, direction: left/right)
	- exit
	*/

  function load(state, tilemapId, groupsToCollideWith) {
		// Add the 
	
		// Load map
		var map = state.add.tilemap(tilemapId);
		state.levelInfo.tilesets.forEach(function(name, index) {
			map.addTilesetImage(name);
		});
		
		// Find the tile IDs requiring collisions
		var collisions = [];
		map.tilesets.forEach(function(element, index) {
			if (element.tileProperties) {
				$.each(element.tileProperties, function(key, prop) {
					if (prop.COLLISION) {
						collisions.push(parseInt(key) + element.firstgid);
					}
				});
			}
		});
		
		// Apply collision & import properties
		state.levelInfo.layers = [];
		map.layers.forEach(function(element, index) {
			if (element.properties.display != "false") {
                var layer = map.createLayer(element.name);				
				
				state.levelInfo.layers[element.name] = layer;
				if (element.properties.collide != "false"){
					//  Set the tiles for collision.
					//  Do this BEFORE generating the p2 bodies below.
					map.setCollision(collisions, true, layer);
					
					var bodies = state.game.physics.p2.convertTilemap(map, layer);
					bodies.forEach(function(body) {
						body.properties = element.properties;
						
						if (groupsToCollideWith) {
							body.setCollisionGroup(state.groups.map);
							groupsToCollideWith.forEach(function(group) {								
								body.collides(group);
							});
						}
					});
				}
				if (Consts.DEBUG.SHOW_BODIES) {
					layer.debug = true;
				}
			}
		});
		
		var groundLayer = state.levelInfo.layers['ground'];
		groundLayer.resizeWorld();
		
		state.game.physics.p2.restitution = 0;
		state.game.physics.p2.gravity.y = Consts.GRAVITY;

        // Fetch player & monsters
		state.levelInfo.objects = [];
		
		createInfoFromObjects(map, "objects", state.levelInfo.objects);	

		return map;
  }
  
  function createInfoFromObjects(map, layerName, result) { 
		var objects = map.objects[layerName];
        if (!objects)
        {
            console.warn('tilemap createInfoFromObjects: Invalid objectgroup name given: ' + layerName);
            return;
        }

        for (var i = 0; i < objects.length; i++)
        {
			var object = objects[i];
			var data = {};
			data.x = object.x;
			data.y = object.y;
			
			for (var j = 0; j < map.tilesets.length; j++){
				var tileset =  map.tilesets[j];
                if (tileset.containsTileIndex(object.gid)){
                    data.properties = tileset.tileProperties[object.gid - tileset.firstgid];
                }
            }
			
			// Override default properties
			for (var attrname in object.properties) { data.properties[attrname] = object.properties[attrname]; }
			
			if (data.properties) {
				if(!result[data.properties.type]) {
					result[data.properties.type] = [];
				}
				result[data.properties.type].push(data);
			}
        }

    }

});
