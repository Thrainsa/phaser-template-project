define(['consts'], function(Consts) {

  return {
    images: [
		'flash',
		'acorn',
		[ 'UI/',
			'bgbottom',
			'bgtop',
			'logo',
			'key',
			'keysmall',
			'shutter',
			'point_ui'
		]
    ],

    tilesets: [
      'tileset',
      'objects'
    ],

    audio: [
      'menu',
	  'cutscene'
    ],

    spritesheet: [
      ['player-idle', 150, 68],
	  ['player-walk', 150, 68],
      ['player-jump', 148, 81]
    ]

  };

});
